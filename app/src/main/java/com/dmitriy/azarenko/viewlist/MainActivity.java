package com.dmitriy.azarenko.viewlist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<Names> namesArray = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        namesArray.add(new Names("Dima", "diktracy@mail"));
        namesArray.add(new Names("Vadim", "solholen@mail"));
        namesArray.add(new Names("Elena", "amandacryst@mail"));
        namesArray.add(new Names("Anna", "solotry@mail"));
        namesArray.add(new Names("Vera", "postapol@mail"));
        namesArray.add(new Names("Alex", "enemyun@mail"));
        namesArray.add(new Names("Artem", "stopthis@mail"));
        namesArray.add(new Names("Valera", "onclick@mail"));
        namesArray.add(new Names("Slava", "alltrue@mail"));
        namesArray.add(new Names("Darina", "selvestro@mail"));

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new MyAdapter(this,namesArray));
        LinearLayout llLay = (LinearLayout) findViewById(R.id.LLID);

        llLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSecondActivity();
            }
        });




    }

    public class MyAdapter extends ArrayAdapter<Names>{

        public MyAdapter(Context context, List<Names> objects) {
            super(context,R.layout.list_item, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = getLayoutInflater().inflate(R.layout.list_item, parent, false);
            TextView name = (TextView) rowView.findViewById(R.id.nameID);
            TextView email = (TextView) rowView.findViewById(R.id.emailID);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageID);
            name.setText(getItem(position).name);
            email.setText(getItem(position).eMail);
            imageView.setImageResource(R.drawable.trybka);
            return rowView;
        }
    }


    public class Names {

        String name;
        String eMail;


        public Names(String name, String eMail) {
            this.name = name;
            this.eMail = eMail;

        }
    }

    public void startSecondActivity(){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("Name", namesArray);
        startActivity(intent);


    }
}
